// Borrowed from Reinhart
var rules;
var ruleshort;
var toperiod;
var autoplay;
var isbreak;
var ispaused;
var isreset;
var istimeout;
var gameduration;
var timeoutduration;
var req = new XMLHttpRequest();
req.onreadystatechange = function(){
  if (this.readyState == 4 && this.status == 200){
    rules = JSON.parse(this.responseText);
  }
};
req.open("GET", "config/rules.json", true);
req.overrideMimeType("application/json"); // Fix Malformed XML error in Firefox
req.send(null);

function startGame(){
  // Confirmation
  if (confirm("Start a new game?\nAny existing progress will be lost.") == true) {
    // Check Competition Type
    var type = document.getElementById("sport").value;
    if (type == "basketball"){
      ruleshort = rules.rules[0]
    } else if (type == "floorball"){
      ruleshort = rules.rules[1]
    } else if (type == "futsal"){
      ruleshort = rules.rules[2]
    };

    // Reset Game State
    ispaused = false;
    istimeout = false;

    // Set First Period
    gameperiod = toperiod = 0;
    document.getElementById("playperiod").innerHTML = 1;
    document.getElementById("breakperiod").innerHTML = 0;
    var durationt = moment.duration(ruleshort.periods[0].duration);
    var durationto = moment.duration(ruleshort.timeouts[0].duration);
    document.getElementById("currentplaytime").innerHTML = isSingleDigit(durationt.minutes(), true) + ":" + isSingleDigit(durationt.seconds(), true);
    document.getElementById("currenttimeout").innerHTML = isSingleDigit(durationto.minutes(), true) + ":" + isSingleDigit(durationto.seconds(), true);
    document.getElementById("hometol").innerHTML = document.getElementById("awaytol").innerHTML = ruleshort.timeouts[0].tol;
    document.getElementById("homescore").innerHTML = document.getElementById("homefoul").innerHTML = document.getElementById("awayscore").innerHTML = document.getElementById("awayfoul").innerHTML = 0;
  } else {
    return 0;
  }
};

function isSingleDigit(number, execute){
  if (number < 10 && execute == true){
    return "0" + number;
  } else {
    return number;
  }
};

// Start Timer
function startTimer(place, duration){
  var clock = document.getElementById("current" + place);
  var counter = 0;
  var durationt = moment.duration(duration);
  clock.innerHTML = isSingleDigit(durationt.minutes(), true) + ":" + isSingleDigit(durationt.seconds(), true);
  var i = setInterval(function(){
    var netduration = duration - (counter + 1) * 1000;
    var durationt = moment.duration(netduration);
    if (inhibitTimer(place) != true){
      clock.innerHTML = isSingleDigit(durationt.minutes(), true) + ":" + isSingleDigit(durationt.seconds(), true);
      counter++
    }

    if(place == "playtime"){
      gameduration = duration - counter * 1000
    }

    // Stop when paused or done
    if(counter === duration / 1000 || isreset == true){
      clearInterval(i);
      if(place == "timeout"){istimeout = false}
      if(isreset == true){
        var durationt = moment.duration(duration);
        clock.innerHTML = isSingleDigit(durationt.minutes(), true) + ":" + isSingleDigit(durationt.seconds(), true);
        if(place == "playtime"){gameduration = 0} else if (place == "timeout"){timeoutduration = 0};
      }
      if(place == "playtime" && counter === duration / 1000){
        changePeriod(1);
        if(autoplay == true){
          gameClock();
        }
      }
    }
  }, 1000);
}

// Check Timer Inhibitor
function inhibitTimer(place){
  // Pause when timeout is running
  if(istimeout == true && place == "playtime"){
    return true
  } else if(ispaused == true){
      return true
  } else {return false}
}

// Toogle Pause Function
function pauseGame(){
  var pausebutton = document.getElementById("pause");
  if (ispaused != true){
    ispaused = true;
    pausebutton.classList.add("bold");
  } else {
    ispaused = false;
    pausebutton.classList.remove("bold");
  }
}

// Reset Button
function resetTimer(){
  if (confirm("Reset Clock?") == true){
    ispaused = true;
    isreset = true;
    ispaused = false;
  }
}

// Toggle Autoplay
function toggleAutoplay(){
  var autoplaybutton = document.getElementById("autoplay");
  if (autoplay == true){
    autoplay = false;
    autoplaybutton.classList.remove("bold")
  }
  else {
    autoplay = true;
    autoplaybutton.classList.add("bold")
  }
}

// Borrowed from jQuery lib
function isNumeric( obj ){
  return !isNaN( parseFloat(obj) ) && isFinite( obj )
};

// Tally Counter
function tallyCounter(place, type, no){
  var ptally = document.getElementById(place + type);
  var ctext = place + "," + type + "," + no;
  if (isNumeric(ptally.innerHTML)){ // Some numbers are replaced by text
    var i;
    if (ptally.innerHTML == 0 && no <= -1){
      return 0 // Prevent negative scores
    } else if (no <= -1) {
      for (i = 0; i < no * -1; i++) {
         ptally.innerHTML -- // Allow negative values such as -1
      };
      console.log(ctext)
    } else {
      for (i = 0; i < no; i++) {
         ptally.innerHTML ++ // Count up
      };
      console.log(ctext)
    }
  } else {
    if (no <= 0){
      ptally.innerHTML = 0 // Prevent errors with negative values
    } else {
      ptally.innerHTML = no; // Immediately add values
      console.log(ctext)
    }
  }
};

// Avoid changing period on game time
function changePeriodAlert(){
  if (gameduration > 0){
    if(confirm("The game period is still running.\nChange period?") == false){
      return false
    } else {
      return true
    }
  } else {return true}
}

// Tally Counter for Game Period
function changePeriod(no){
  var i;
  var playperiod = document.getElementById("playperiod");
  var breakperiod = document.getElementById("breakperiod");
  var gameclock = document.getElementById("currentplaytime");
  var gameclocktext = document.getElementById("playtimetext");
  // Reset clock
  ispaused = true;
  isreset = true;
  ispaused = false;

  if (changePeriodAlert() == true){
    if (gameperiod == 0 && no <= -1){
      return 0;
    } else if (gameperiod == ruleshort.periods.length - 1 && no >= 1){
      return 0;
    } else if (no <= -1){
      for (i = 0; i <  no * -1; i++){
        gameperiod --; // Allow negative values
      }
    } else if (no >= 1){
      for (i = 0; i < no; i++) {
        gameperiod ++; // Count up
      }
    }

    // Change Tally
    var newperiod = ruleshort.periods[gameperiod].id;
    if (newperiod.charAt(0) == "e"){
      playperiod.innerHTML = newperiod;
      gameclocktext.innerHTML = "Extra";
    } else if (newperiod.charAt(0) == "b"){
      breakperiod.innerHTML = newperiod.charAt(1);
      gameclock.classList.remove("green-1");
      gameclock.classList.add("red-1");
      gameclocktext.innerHTML = "Break";
    } else if (newperiod.charAt(0) == "p"){
      playperiod.innerHTML = newperiod.charAt(1);
      gameclock.classList.remove("red-1");
      gameclock.classList.add("green-1");
      gameclocktext.innerHTML = "Time";
    }

    var durationt = moment.duration(ruleshort.periods[gameperiod].duration);
    //var durationto = moment.duration(ruleshort.timeouts[toperiod].duration);
    document.getElementById("currentplaytime").innerHTML = isSingleDigit(durationt.minutes(), true) + ":" + isSingleDigit(durationt.seconds(), true);
    //document.getElementById("currenttimeout").innerHTML = isSingleDigit(durationto.minutes(), true) + ":" + isSingleDigit(durationto.seconds(), true);

  }
}

// Game Clock
function gameClock(){
  var duration = ruleshort.periods[gameperiod].duration;
  isreset = false;
  startTimer("playtime", duration);
}

// Timeout
function timeOut(place){
  var duration = ruleshort.timeouts[gameperiod].duration;
  if (isbreak != true){
    tallyCounter(place, "tol", -1);
    startTimer("timeout", duration);
    istimeout = true;
  }
}

// Team goal
function goalTo(place, score){
  tallyCounter(place, "score", score)
};

// Team foul
function teamFoul(place, no){
  tallyCounter(place, "foul", no)
};

// Team's time-outs left
function teamTol(place, no){
  tallyCounter(place, "tol", no)
};
